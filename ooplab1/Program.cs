﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geometry {
    /// <summary>
    ///     Итератор для 2д-фигур             
    /// </summary>
    /// <typeparam name="T">2д-фигура</typeparam>
    class ShapeEnumerator<T> : IEnumerator<T> where T : Shape2d {
        private ShapeCollection<T> collection;
        private int pointer;

        public ShapeEnumerator(ShapeCollection<T> _collection) {
            collection = _collection;
            pointer = -1;
        }

        public T Current {
            get {
                if (pointer == -1 || pointer == collection.Count)
                    throw new NotImplementedException();
                return collection[pointer];
            }
        }

        object IEnumerator.Current {
            get {
                return Current;
            }
        }

        public void Dispose() {
        }

        public bool MoveNext() {
            if (pointer < collection.Count)
                pointer++;
            return !(pointer == collection.Count);
        }

        public void Reset() {
            pointer = -1;
        }
    }

    /// <summary>
    ///     Коллекция 2д-фигур
    /// </summary>
    /// <typeparam name="T">2д-фигура</typeparam>
    class ShapeCollection<T> : ICollection<T> where T : Shape2d {
        private List<T> innerCol;
        public ShapeCollection() {
            innerCol = new List<T>();
        }

        public int Count {
            get {
                return innerCol.Count;
            }
        }

        public bool IsReadOnly {
            get {
                return false;
            }
        }

        public void Add(T item) {
            if (!Contains(item))
                innerCol.Add(item);
        }

        public void Clear() {
            innerCol.Clear();
        }

        public bool Contains(T item) {
            bool found = false;

            foreach (T curItem in innerCol) {
                if (item.Area() == curItem.Area()) {
                    found = true;
                }
            }

            return found;
        }

        public void CopyTo(T[] array, int arrayIndex) {
            if (array == null)
                throw new ArgumentNullException("The array cannot be null.");
            if (arrayIndex < 0)
                throw new ArgumentOutOfRangeException("The starting array index cannot be negative.");
            if (Count > array.Length - arrayIndex + 1)
                throw new ArgumentException("The destination array has fewer elements than the collection.");

            for (int i = 0; i < innerCol.Count; i++) {
                array[i + arrayIndex] = innerCol[i];
            }
        }

        public T this[int index] {
            get { return innerCol[index]; }
            set { innerCol[index] = value; }
        }

        public IEnumerator<T> GetEnumerator() {
            return new ShapeEnumerator<T>(this);
        }

        public bool Remove(T item) {
            bool result = false;

            for (int i = 0; i < innerCol.Count; i++) {
                T curItem = innerCol[i];

                if (curItem.Area() == item.Area() &&
                    curItem.Perimeter() == item.Perimeter()) {
                    result = true;
                    innerCol.RemoveAt(i);
                    break;
                }
            }

            return result;
        }

        IEnumerator IEnumerable.GetEnumerator() {
            return new ShapeEnumerator<T>(this);
        }

        public delegate void Sorter();

        public void MySort(Sorter method) {
            method();
        }

        private static int cmp(T x, T y) {
            return (x.Area() == y.Area() ? 0 : x.Area() > y.Area() ? 1 : -1);
        }

        public void IntroSort() {
            int maxDepth = (int) Math.Log(Count);
            quickSort(0, Count - 1, maxDepth);
        }

        public void quickSort(int l, int r, int depth) {
            if (depth == 0) {
                Console.WriteLine("ERROR IN INTROSORT!!!!!!");
                //  HeapSort
                
                return;
            }
            T x = innerCol[l + (r - l) / 2];
            int i = l;
            int j = r;
            while (i <= j) {
                while (innerCol[i].Compare(x)) i++;
                while (x.Compare(innerCol[j])) j--;
                if (i <= j) {
                    T temp = innerCol[i];
                    innerCol[i] = innerCol[j];
                    innerCol[j] = temp;
                    i++;
                    j--;
                }
            }
            if (i < r)
                quickSort(i, r, depth--);

            if (l < j)
                quickSort(l, j, depth--);
        }
    }

    /// <summary>
    /// Интерфейс 2д-фигур
    /// </summary>
    public interface Shape2d : ICloneable {
        double Area();
        double Perimeter();
        bool Compare(Shape2d right);
    }

    /// <summary>
    /// Фабрика 3д-фигур
    /// </summary>
    abstract class Shape3dFactory {
        public abstract Shape2d SectionXY();
        public abstract Shape2d SectionYZ();
    }

    /// <summary>
    /// Прямоугольник - является 2д-фигурой
    /// </summary>
    class Rectangle : Shape2d {
        private double _sideA, _sideB;

        public object Clone() {
            return new Rectangle(_sideA, _sideB);
        }

        public Rectangle(double a, double b) {
            _sideA = a;
            _sideB = b;
        }

        public Rectangle() {
            _sideA = 0;
            _sideB = 0;
        }

        public double Area() {
            return _sideA * _sideB;
        }

        public double Perimeter() {
            return 2 * _sideA * _sideB;
        }

        public bool Compare(Shape2d right) {
            return this.Area() > right.Area();
        }
    }

    /// <summary>
    /// Круг - является 2д-фигурой
    /// </summary>
    class Circle : Shape2d {
        private double _radius;
        public double radius {
            get { return _radius; }
            set { _radius = value; }
        }
        private const double Pi = Math.PI;

        public Circle(double r) {
            _radius = r;
        }

        public object Clone() {
            return new Circle(radius);
        }

        public Circle() {
            _radius = 0;
        }

        public double Area() {
            return Pi * _radius * _radius;
        }

        public double Perimeter() {
            return Pi * 2 * _radius;
        }

        public bool Compare(Shape2d right) {
            return this.Area() > right.Area();
        }
    }

    /// <summary>
    /// Треугольник - является 2д-фигурой
    /// </summary>
    class Triangle : Shape2d {
        public double a { get; set; }
        public double b { get; set; }
        public double c { get; set; }

        public Triangle(double _a, double _b, double _c) {
            a = _a;
            b = _b;
            c = _c;
        }

        public object Clone() {
            return new Triangle(a, b, c);
        }

        public double Area() {
            double p = (a + b + c) / 2;
            return Math.Sqrt(p * (p - a) * (p - b) * (p - c));
        }

        public double Perimeter() {
            return a + b + c;
        }

        public bool Compare(Shape2d right) {
            return this.Area() > right.Area();
        }

        public delegate void Del(string message);

        public void MethodWithCallback(string[] args, Del callback) {
            string toDo = "";
            foreach(string s in args) {
                toDo += s + (s == args.Last<string>() ? "" : " ");
            }
            callback(toDo);
        }

        public void logger(string message) {
            Console.WriteLine(message);
        }

        public void reverseLogger(string message) {
            char[] tmp = message.ToCharArray();
            Array.Reverse(tmp);
            Console.WriteLine(tmp);
        }
    }

    /// <summary>
    /// Фабрика сфер, является фабрикой 3д-фигур
    /// </summary>
    class SphereFactory : Shape3dFactory {
        public override Shape2d SectionXY() {
            return new Circle();
        }

        public override Shape2d SectionYZ() {
            return new Circle();
        }
    }

    /// <summary>
    /// Фабрика цилиндров, является фабрикой 3д-фигур
    /// </summary>
    class CylinderFactory : Shape3dFactory {
        public override Shape2d SectionXY() {
            return new Rectangle();
        }

        public override Shape2d SectionYZ() {
            return new Circle();
        }
    }

    /// <summary>
    /// Фабрика кубов, является фабрикой 3д-фигур
    /// </summary>
    class CubeFactory : Shape3dFactory {
        public override Shape2d SectionXY() {
            return new Rectangle();
        }
        public override Shape2d SectionYZ() {
            return new Rectangle();
        }
    }

    /// <summary>
    /// 3д фигура
    /// </summary>
    class Shape3d {
        public Shape2d SectionXY;
        public Shape2d SectionYZ;

        public Shape3d(Shape3dFactory factory) {
            SectionXY = factory.SectionXY();
            SectionYZ = factory.SectionYZ();
        }
    }


    class Program {
        static void Main(string[] args) {
            Triangle triangle = new Triangle(3, 4, 5);
            List<string> arg = new List<string>();
            arg.Add(triangle.Area().ToString());
            arg.Add(triangle.a.ToString());
            triangle.MethodWithCallback(arg.ToArray(), triangle.logger);
            triangle.MethodWithCallback(arg.ToArray(), triangle.reverseLogger);

            Action<string> printMessage;

            //Func<string, string> convert = delegate (string s) {
            //    return s.ToUpper();
            //};

            Func<string, string> convert;

            if (triangle.a == 5) {
                printMessage = delegate (string s) { Console.WriteLine(s); };
                convert = toUp;
            } else {
                printMessage = delegate (string s) { Console.WriteLine("[ {0} ]", s); };
                convert = toLow;
            }
            printMessage(convert("I am Action<string>"));

            ShapeCollection<Triangle> triangles = new ShapeCollection<Triangle>();
            triangles.Add(new Triangle(4, 5, 6));
            triangles.Add(new Triangle(2, 2, 1));
            triangles.Add(new Triangle(2, 2, 3));
            triangles.Add(new Triangle(5, 5, 5));
            triangles.Add(new Triangle(2, 2, 3));
            triangles.Add(new Triangle(1, 1, 1));
            triangles.Add(new Triangle(2, 3, 4));
            triangles.Add(new Triangle(5, 26, 28));
            triangles.Add(new Triangle(5, 26, 26));


            triangles.MySort(triangles.IntroSort);
            foreach (Triangle t in triangles) {
                Console.WriteLine("{0} - area", t.Area(), t.a, t.b, t.c);
            }
        }

        private static string toUp(string s) {
            return s.ToUpper();
        }
        private static string toLow(string s) {
            return s.ToLower();
        }
    }
}
